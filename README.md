# After Effects Project Setup

A small custom project setup tool for Adobe After Effects.

Goal:
* Simplify per episode project setup in a large tv series production.
* Provide quick access to common expressions used in production.

![screenshot](https://codeberg.org/n0mad/After_Effects_Project_Setup/raw/branch/master/ToolCapture.jpg)