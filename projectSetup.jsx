var scriptTitle = "Project Set-up";
var intrPalette = buildUI(this);

if (intrPalette != null && intrPalette instanceof Window)
{
    intrPalette.show();
}

function buildUI (thisObject)
{
    if (thisObject instanceof Panel)
        intrPalette = thisObject;
    else
        intrPalette = new Window ("palette",[scriptTitle, undefined,{resizeable: true}]);
    
    if (intrPalette != null)
    {
        var palRes = 
        "Group {\
            orientation: 'column',\
            alignment: ['fill', 'fill'],\
            alignChildren: ['fill', 'top'],\
            mainGrp: Group {orientation: 'column',\
                mainTitle: StaticText {text: 'Comp Project Setup', alignment: ['center', 'top']}, \
                sepTitle: StaticText {text: '----------------------', alignment: ['center', 'top']}, \
                createStructGrp: Panel {orientation : 'row', alignment:['fill', 'center']\
                    text: 'Create Folder Structure', \
                    createFolders: Button {text: 'Create Folders', alignment: ['center', 'top']}, \
                }\
                createCompGrp: Panel {orientation : 'row', alignment:['fill', 'center']\
                    text: 'Animation comp name', \
                    createCompName: EditText {text: '', alignment: ['fill', 'top']},\
                    createAnimComp: Button {text: 'Create Anim Comp', alignment: ['right', 'top']}, \
                }\
                createNullGrp: Panel {orientation : 'column', alignment:['fill', 'center']\
                    text: 'Create control null', \
                    charMainText: StaticText {text: 'Set main character amount:', alignment: ['left', 'top']},\
                    addMainCharCount: EditText {text: '', alignment: ['fill', 'top']},\
                    nllMainText: StaticText {text: 'Null name:', alignment: ['left', 'top']},\
                    createNullName: EditText {text: '', alignment: ['fill', 'top']},\
                    createCtrlNull: Button {text: 'Create Control null', alignment: ['right', 'top']}, \
                }\
                addExprGrp: Panel {orientation: 'row', alignment: ['fill', 'center'],\
                    text: 'Utilty Expressions', \
                    fsBtn: Button {text: 'Flip Scale', alignment: ['left', 'top']}, \
                }\
                sepTitle: StaticText {text: '----------------------', alignment: ['center', 'top']}, \
                infoMain: Button {text: 'About', alignment: ['center', 'top']}\
            },\
        }";

        intrPalette.grp = intrPalette.add(palRes);
        intrPalette.layout.layout(true);
        intrPalette.layout.resize();

        intrPalette.grp.mainGrp.createStructGrp.createFolders.onClick = function ()
        {
            app.beginUndoGroup("Folder Setup");
            CreateFolderStructure();
            app.endUndoGroup;
        };

        intrPalette.grp.mainGrp.createCompGrp.createAnimComp.onClick = function ()
        {
            CreateMasterComp();
            PopulateComp();
        };

        intrPalette.grp.mainGrp.createNullGrp.createCtrlNull.onClick = function ()
        {
            NullAdd();
        };

        intrPalette.grp.mainGrp.infoMain.onClick = function ()
        {
            DisplayInfo();
        };
        
        intrPalette.grp.mainGrp.addExprGrp.fsBtn.onClick = function ()
        {
            FlipScale();
        };

        intrPalette.onResizing = intrPalette.onRize = function ()
        {
            this.layout.resize();
        };
        
    }
    return intrPalette;
}

function CreateFolderStructure ()
{
    var currentParent = app.project.rootFolder;

    var propFolder = app.project.items.addFolder("00_Props");
    propFolder.parentFolder = currentParent;
    var animaticFolder = app.project.items.addFolder("02_Animatic");
    animaticFolder.parentFolder = currentParent;
    var charFolder = app.project.items.addFolder("03_Characters");
    charFolder.parentFolder = currentParent;
    var bgFolder = app.project.items.addFolder("04_Backgrounds");
    bgFolder.parentFolder = currentParent;
    var compFolder = app.project.items.addFolder("05_Comps");
    compFolder.parentFolder = currentParent;
    alert("Folder structure created!");
}

function GetCurrentComp ()
{
    var xComp = app.project.activeItem;
    if (xComp == undefined)
    {
        alert("please select a comp before!");
        return null;
    }

    return xComp;
}

function GetAllLayers ()
{
    var allComp = GetCurrentComp();
    var lyrsList = new Array();
    if(!(allComp instanceof CompItem))
        return null;

    for(var i = 1; i<=allComp.numLayers; i++)
    {
        lyrsList[lyrsList.length] = allComp.layer(i).name;
    }
    return lyrsList;
}

function NullAdd ()
{
    var comp = GetCurrentComp();
    comp == null ? alert("Please, select a composition") : NullNoLayer();
}

function NullNoLayer ()
{
    app.beginUndoGroup("Null Setup");
        var project = app.project;
        var comp = project.activeItem;
        var userNullName = intrPalette.grp.mainGrp.createNullGrp.createNullName.text;
        var mcCount = parseInt(intrPalette.grp.mainGrp.createNullGrp.addMainCharCount.text);
        var masterNull = comp.layers.addNull();
        masterNull.name = userNullName;
        var flipBool = masterNull.effect.addProperty ("ADBE Checkbox Control");
        flipBool.name = 'FlipBG';
        var fillCol = masterNull.effect.addProperty ("ADBE Color Control");

        for (var mc = 0; mc < mcCount; mc++)
        {
            masterNull.effect.addProperty ("ADBE Checkbox Control");
        }
        
        alert("null " + userNullName + " created");
    app.endUndoGroup;
}

function CreateMasterComp ()
{
    app.beginUndoGroup("Comp Creation");
        var compTitle = intrPalette.grp.mainGrp.createCompGrp.createCompName.text;
        var compFolderInside = GetCompFolder();
        
        //Automatic comp size/fps
        var compWidth = 1920;
        var compHeight = 1080;
        var compFPS = 25;
        var compDuration = Math.round(GetAnimatic().duration) + 1;
    
        compFolderInside.items.addComp(compTitle, compWidth, compHeight, 1, compDuration, compFPS);

        alert("comp " + compTitle + " created");
    app.endUndoGroup;
}

function GetAnimatic ()
{
    var animName = "02_Animatic";
    //var animName = animaticFolder.name;
    var animLocation;
    for (var a = 1; a <= app.project.items.length; a++)
    {
        var itemSelector = app.project.item(a);
        if (itemSelector.name == animName && itemSelector instanceof FolderItem)
        {
            animLocation = itemSelector;
        }
    }

    return animLocation.item(1);
}

function GetCompFolder ()
{
    var compFolderName = "05_Comps";
    //var compFolderName = compFolder.name;
    var compFolderLoc;
    for(var c = 1; c <= app.project.items.length; c++)
    {
        var compFolSelector = app.project.item(c);
        if (compFolSelector.name == compFolderName && compFolSelector instanceof FolderItem)
        {
            compFolderLoc = compFolSelector;
        }
    }
    return compFolderLoc; 
}

function PopulateComp ()
{
    var mainCompLoc;
    var compName = intrPalette.grp.mainGrp.createCompGrp.createCompName.text;
    for(var m = 1; m <= app.project.items.length; m++)
    {
        var mainCompSelector = app.project.item(m);
        if(mainCompSelector.name == compName && mainCompSelector instanceof CompItem)
        {
            mainCompLoc = mainCompSelector;
        }
    }
    var animVid = mainCompLoc.layers.add(GetAnimatic());
    animVid;
    animVid.position.setValue([284.0, 168.0, 0.0]);
    animVid.scale.setValue([75.0, 75.0]);
}

function DisplayInfo ()
{
    var infoWinMain = new Window("window", "About", [0, 0, 300, 100]);
    var reminder = "Episode Setup Toolkit v0.1 \nDeveloped by Samuel C. Palafox";
    infoWinMain.add("image", undefined, "C:/Program Files/Adobe/Adobe After Effects CC 2018/Support Files/PNG/a@t.png");
    var text = infoWinMain.add("statictext", [0, 0, 300, 75], reminder, {multiline: true});
    

    infoWinMain.layout.layout(true);
    infoWinMain.show();
    infoWinMain.center();
    infoWinMain.onResizing = infoWinMain.onRize = function() 
    {
        this.layout.resize();
    };
}

function FlipScale ()
{
    /*  *display layers in list
        *get selected layer in list as null
        *get name input as effect
        *display layers in list
        *get selected layer.scale in list as target*/
    var fsMainWin = new Window("window", "Expressions", [0, 0, 300, 100]);
    var exprPanel = fsMainWin.add("panel", undefined, "Flip Scale");
    var exprMainGroup = exprPanel.add("group", undefined, undefined);
    var fsGroup = exprMainGroup.add("group", undefined, undefined);
        fsGroup.orientation = "column";
    fsGroup.add("statictext", undefined, "Select Anim Control");
    var nllLayer = fsGroup.add("DropdownList", undefined, GetAllLayers());
        nllLayer.selection = 0;
    fsGroup.add("statictext", undefined, "Mark Effect Name");
    var fxlName = fsGroup.add("edittext", undefined, '');
        fxlName.alignment = ["fill", "center"];
    fsGroup.add("statictext", undefined, "Select Target Layer");
    var tgtLayer = fsGroup.add("DropdownList", undefined, GetAllLayers());
        tgtLayer.selection = 0;
    var fsAplBtn = fsGroup.add("button", undefined, "Apply");

    fsMainWin.layout.layout(true);
    fsMainWin.show();
    fsMainWin.center();
    fsMainWin.onResizing = fsMainWin.onRize = function() 
    {
        this.layout.resize(false);
    };

    //selection.name resolves to undefined!
    fsAplBtn.onClick = function ()
    {
        alert("Null " + nllLayer.selection.name + " with effect " + fxlName.text + " to be linked in " + tgtLayer.selection.name + " scale expression");
    }
}

function ScaleExpression ()
{
    /*
        x = transform.scale[0];
        y = transform.scale[1];
        comp("COMP_NAME").layer("NULL_NAME").effect("EFFECT_NAME")("Checkbox") != true ? [x,y] : [-x,y];
    */
}